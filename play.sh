#!/bin/zsh

cd ${0:A:h}

ansible-playbook \
	-i localhost, \
	-c local \
	-u $USER \
	--become-password-file=op-become-pass.sh \
	$@ \
	playbook.yml
